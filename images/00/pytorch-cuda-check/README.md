# Disk benchmark tool

Info: https://github.com/leeliu/dbench

## Sample local run

Note: Run this in an empty folder!

```sh
docker run --rm \
    --name dbench \
    --mount type=bind,source="$(pwd)",target=/data \
    registry.gitlab.com/sungazer-pub/docker-images/dbench:1.0
```