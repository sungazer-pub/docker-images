#! /bin/python

from time import sleep
import torch

print('Is cuda available ? {}'.format(torch.cuda.is_available()))
if torch.cuda.is_available():
    print('Cuda device: {}'.format(torch.cuda.get_device_name(0)))

sleep(3600.0)
exit(0)